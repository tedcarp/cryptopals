#pragma once

#include <Common.h>

class StringUtils {
  public:
    static bool is_printable(const string &string);

    static string binary_to_string(const vector<byte> &binary);
    static vector<byte> string_to_binary(const string &string);
};