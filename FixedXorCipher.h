#pragma once

#include <Common.h>

class FixedXorCipher {
  public:
    vector<string> get_possible_solutions(const vector<byte> &encrypted_message);
    char break_key(const vector<byte> &encrypted_message);
};
