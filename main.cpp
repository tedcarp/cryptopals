#include <Common.h>
#include <Base64Encoder.h>
#include <BinaryOperator.h>
#include <FixedXorCipher.h>
#include <HexCoder.h>
#include <MessageAnalyzer.h>
#include <RepeatingXorCipher.h>
#include <StringUtils.h>

static void set1_challenge1()
{
    const string hex_input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    HexCoder coder;
    vector<byte> binary_array = coder.hex_string_to_binary(hex_input);
    Base64Encoder encoder;
    string base64 = encoder.binary_to_base64(binary_array);
    printf("Set 1/Challenge 1| base64: %s\n", base64.c_str());
}

static void set1_challenge2()
{
    const string hex_input = "1c0111001f010100061a024b53535009181c";
    const string hex_input2 = "686974207468652062756c6c277320657965";
    HexCoder coder;
    vector<byte> binary_array = coder.hex_string_to_binary(hex_input);
    vector<byte> binary_array2 = coder.hex_string_to_binary(hex_input2);
    BinaryOperator op;
    vector<byte> xor_array = op.xor_arrays(binary_array, binary_array2);
    string xor_hex = coder.binary_to_hex_string(xor_array);
    printf("Set 1/Challenge 2| fixed XOR: %s\n", xor_hex.c_str());
}

static void set1_challenge3()
{
    const string hex_input = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    HexCoder coder;
    vector<byte> binary_input = coder.hex_string_to_binary(hex_input);

    FixedXorCipher cipher;
    vector<string> solutions = cipher.get_possible_solutions(binary_input);

    MessageAnalyzer analyzer;
    string message = analyzer.find_best_english_string(solutions);
    printf("Set 1/Challenge 3| single-byte XOR cipher: %s\n", message.c_str());
}

static void set1_challenge4()
{
    const string all_inputs[] = {
        #include "Set1Challenge4Data.h"
    };

    vector<string> all_solutions;
    for (auto input : all_inputs) {
        HexCoder coder;
        vector<byte> binary_input = coder.hex_string_to_binary(input);

        FixedXorCipher cipher;
        vector<string> solutions = cipher.get_possible_solutions(binary_input);
        all_solutions.insert(all_solutions.end(), solutions.begin(), solutions.end());
    }

    MessageAnalyzer analyzer;
    string message = analyzer.find_best_english_string(all_solutions);
    printf("Set 1/Challenge 4| detect single-byte XOR: %s\n", message.c_str());
}

static void set1_challenge5()
{
    const string input = "Burning 'em, if you ain't quick and nimble I go crazy when I hear a cymbal";
    const string key = "ICE";

    RepeatingXorCipher cipher;
    vector<byte> encrypted  = cipher.encode(input, key);

    HexCoder hex;
    string encrypted_hex = hex.binary_to_hex_string(encrypted);

    printf("Set 1/Challenge 5| encoding repeating-key XOR: %s\n", encrypted_hex.c_str());
}

static void set1_challenge6()
{
    const string input =
        #include "Set1Challenge6Data.h"
        ;

    Base64Encoder coder;
    vector<byte> binary_input = coder.base64_to_binary(input);

    RepeatingXorCipher cipher;
    string key = cipher.break_key(binary_input);
    string message = cipher.decode(binary_input, key);

    printf(
        "Set 1/Challenge 6| break repeating-key XOR: key: %s \n%s\n",
        key.c_str(), message.c_str());
}

int main()
{
    set1_challenge1();
    set1_challenge2();
    set1_challenge3();
    set1_challenge4();
    set1_challenge5();
    set1_challenge6();
    return 0;
}
