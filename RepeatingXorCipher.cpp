#include <BinaryOperator.h>
#include <RepeatingXorCipher.h>
#include <FixedXorCipher.h>
#include <StringUtils.h>

string get_repeated_key(const string &key, unsigned int length)
{
    string repeated_key;
    for (unsigned int i = 0; i < length; i++) {
        repeated_key.push_back(key[i % key.length()]);
    }
    return repeated_key;
}

vector<byte> RepeatingXorCipher::encode(const string &message, const string &key)
{
    string repeated_key = get_repeated_key(key, message.length());

    vector<byte> binary_message = StringUtils::string_to_binary(message);
    vector<byte> binary_repeated_key = StringUtils::string_to_binary(repeated_key);
    BinaryOperator op;
    return op.xor_arrays(binary_message, binary_repeated_key);
}

string
RepeatingXorCipher::decode(
    const vector<byte> &encrypted_message, const string &key)
{
    string repeated_key = get_repeated_key(key, encrypted_message.size());
    vector<byte> binary_repeated_key = StringUtils::string_to_binary(repeated_key);
    BinaryOperator op;
    return StringUtils::binary_to_string(
        op.xor_arrays(encrypted_message, binary_repeated_key));
}

static vector<vector<byte>> break_into_chunks(const vector<byte> &data, unsigned int chunk_size)
{
    vector<vector<byte>> chunks;

    vector<byte> current_chunk;
    for (byte b : data) {
        current_chunk.push_back(b);
        if (current_chunk.size() == chunk_size) {
            chunks.push_back(current_chunk);
            current_chunk.clear();
        }
    }

    // Only run over 10 chunks to save time.
    if (chunks.size() > 10)
        chunks.resize(10);

    return chunks;
}

static double average_hamming_distance(const vector<vector<byte>> &data)
{
    unsigned int combinations = 0;
    double total_hamming_distance = 0.0;
    for (unsigned int i = 0; i < data.size(); i++) {
        for (unsigned int j = i + 1; j < data.size(); j++) {
            BinaryOperator op;
            double hamming_distance = op.hamming_distance(data[i], data[j]);
            total_hamming_distance += hamming_distance;
            combinations++;
        }
    }
    return total_hamming_distance / combinations;
}

static unsigned int break_key_size(const vector<byte> &data)
{
    static const unsigned int min_key_size = 2;
    static const unsigned int max_key_size = 40;
    unsigned int best_key_size = 0;
    double lowest_hamming_distance_pet_byte = 0;
    for (unsigned int key_size = min_key_size; key_size <= max_key_size; key_size++) {
        vector<vector<byte>> key_length_chunks = break_into_chunks(data, key_size);
        double hamming_distance_per_byte =
            average_hamming_distance(key_length_chunks) / key_size;
        if (hamming_distance_per_byte < lowest_hamming_distance_pet_byte || best_key_size == 0) {
            best_key_size = key_size;
            lowest_hamming_distance_pet_byte = hamming_distance_per_byte;
        }
    }

    return best_key_size;
}

static vector<vector<byte>> transpose_into_rotated(const vector<byte> &data, unsigned int key_size)
{
    vector<vector<byte>> all;
    all.resize(key_size);
    unsigned index = 0;
    for (byte b : data) {
        all[index % key_size].push_back(b);
        index++;
    }

    return all;
}

string RepeatingXorCipher::break_key(const vector<byte> &encrypted_message)
{
    unsigned int key_size = break_key_size(encrypted_message);
    vector<vector<byte>> transposed = transpose_into_rotated(encrypted_message, key_size);
    string key;
    for (vector<byte> &position : transposed) {
        string transposed_string = StringUtils::binary_to_string(position);
        FixedXorCipher cipher;
        key.push_back(cipher.break_key(position));
    }
    return key;
}
