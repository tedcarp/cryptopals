#pragma once

#include <Common.h>

class BinaryOperator {
public:
    vector<byte> xor_arrays(const vector<byte> &first, const vector<byte> &second);
    unsigned int hamming_distance(const vector<byte> &first, const vector<byte> &second);
};
