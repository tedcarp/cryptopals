#include <stdexcept>

#include <Base64Encoder.h>

static const string base64_map("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");

static char base64_char_for_value(unsigned int value)
{
    if (value > 63) {
        throw invalid_argument("value too large to encode in base64");
    }

    return base64_map[value];
}

static unsigned int value_for_bas64_char(char c)
{
    size_t index = base64_map.find(c);
    if (index == string::npos)
        throw invalid_argument("Not a valid base64 char");

    return index;
}

static unsigned int bit_value_at_index(const vector<byte> &binary_data, unsigned int bit_index)
{
    if (bit_index > binary_data.size() * 8)
        return 0;

    // Get the byte containing the bit.
    byte containing_bit = binary_data[bit_index / 8];

    // Bitwise intersection of the byte and only the bit we care about.
    byte intersection = containing_bit & (1 << ( 7 - (bit_index % 8)));

    // Return 1 or 0 depending on whether the intersection produce non-zero value.
    return intersection ? 1 : 0;
}

static unsigned int six_bit_value_at_index(const vector<byte> &binary_data, unsigned int bit_index)
{
    unsigned int value = 0;
    for (unsigned int i = 0; i < 6; i++) {
        value = value << 1;
        value += bit_value_at_index(binary_data, bit_index + i);
    }
    return value;
}

static string pad_base64_encoding(const string &base64_string)
{
    string padded_string = base64_string;
    unsigned int padding_characters = (base64_string.size() % 4 == 0) ? 0 : 4 - (base64_string.size() % 4);
    for (unsigned int i = 0; i < padding_characters; i++) {
        padded_string.push_back('=');
    }

    return padded_string;
}

string Base64Encoder::binary_to_base64(const vector<byte> &binary_data)
{
    string encoded_string;
    unsigned int bit_index = 0;
    while (bit_index < binary_data.size() * 8) {
        unsigned int value_at_index = six_bit_value_at_index(binary_data, bit_index);
        encoded_string.push_back(base64_char_for_value(value_at_index));
        bit_index += 6;
    }

    encoded_string = pad_base64_encoding(encoded_string);
    return encoded_string;
}

unsigned int decode_4_chars(const string &chars)
{
    if (chars.length() != 4)
        throw invalid_argument("must decode 4 chars at a time");

    unsigned int value = 0;
    for (char c : chars) {
        unsigned int to_add = 0;
        if (c != '=')
            to_add = value_for_bas64_char(c);
        value = value << 6;
        value += to_add;
    }

    return value;
}

unsigned int count_equals(const string& chars)
{
    unsigned int count = 0;
    for (char c : chars) {
        if (c == '=')
            count++;
    }

    return count;
}

vector<byte> decode_4_chars_to_3_bytes(const string& chars)
{
    unsigned int bytes_as_int = decode_4_chars(chars);
    vector<byte> bytes;
    for (unsigned int i = 0; i < 3 - count_equals(chars); i++) {
        unsigned int byte = (bytes_as_int >> ((2 - i) * 8)) & 0xFF;
        bytes.push_back(byte);
    }

    return bytes;
}

vector<byte> Base64Encoder::base64_to_binary(const string &input)
{
    vector<byte> bytes;
    for (unsigned int index = 0; index < input.length(); index += 4) {
        string four_chars;
        for (unsigned int offset = 0; offset < 4; offset++) {
            four_chars.push_back(input[index + offset]);
        }

        vector<byte> three_bytes = decode_4_chars_to_3_bytes(four_chars);
        bytes.insert(bytes.end(), three_bytes.begin(), three_bytes.end());
    }

    return bytes;
}
