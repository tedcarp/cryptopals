#include <StringUtils.h>

bool StringUtils::is_printable(const string &string)
{
    for (char c : string) {
        if (!isprint(c) && !isspace(c)) {
            return false;
        }
    }

    return true;
}

string StringUtils::binary_to_string(const vector<byte> &binary)
{
    return string(reinterpret_cast<const char *>(&binary[0]), binary.size());
}

vector<byte> StringUtils::string_to_binary(const string &string)
{
    vector<byte> binary;
    for (char c : string) {
        binary.push_back(static_cast<byte>(c));
    }
    return binary;
}