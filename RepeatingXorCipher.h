#pragma once

#include <Common.h>

class RepeatingXorCipher {
  public:
    vector<byte> encode(const string &message, const string &key);
    string decode(const vector<byte> &encrypted_message, const string &key);
    string break_key(const vector<byte> &encrypted_message);
};
