#include <stdexcept>

#include <HexCoder.h>

byte hex_char_to_value(char c)
{
    if (c >= '0' && c <= '9') {
        return c - '0';
    }

    if (c >= 'a' && c <= 'f') {
        return c - 'a' + 10;
    }

    if (c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    }

    throw invalid_argument("not a hex value");
}

vector<byte> HexCoder::hex_string_to_binary(const string &hex_string)
{
    vector<byte> binary_values;
    string::const_iterator iterator = hex_string.begin();
    while (iterator != hex_string.end()) {
        // Get the next two characters to form a byte.
        char first = *iterator;
        iterator++;

        // If only one character is available, pad with zero.
        char second = '0';
        if (iterator != hex_string.end()) {
            second = *iterator;
            iterator++;
        }

        // Compute the binary value of the two chars.
        byte first_value = hex_char_to_value(first);
        byte second_value = hex_char_to_value(second);
        byte total_value = (first_value << 4) + second_value;

        binary_values.push_back(total_value);
    }

    return binary_values;
}

string HexCoder::binary_to_hex_string(const vector<byte> &binary_data)
{
    string hex_string;
    for (byte byte : binary_data) {
        char hex_byte[3];
        sprintf(hex_byte, "%hhx", byte);
        hex_string.append(hex_byte);
    }
    return hex_string;
}