#pragma once

#include <Common.h>

class Base64Encoder {
  public:
    string binary_to_base64(const vector<byte> &binary_data);
    vector<byte> base64_to_binary(const string &input);
};
