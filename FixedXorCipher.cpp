#include <BinaryOperator.h>
#include <FixedXorCipher.h>
#include <MessageAnalyzer.h>
#include <StringUtils.h>

vector<string> FixedXorCipher::get_possible_solutions(
    const vector<byte> &encrypted_message)
{
    vector<string> decoded_strings;
    byte key = 0;
    while (true) {
        vector<byte> cipher_vec(encrypted_message.size(), key);
        BinaryOperator op;
        vector<byte> xor_vec = op.xor_arrays(encrypted_message, cipher_vec);
        string candidate = StringUtils::binary_to_string(xor_vec);
        if (StringUtils::is_printable(candidate)) {
            decoded_strings.emplace_back(move(candidate));
        }

        if (key == 255)
            break;

        key++;
    }

    return decoded_strings;
}

static map<char, string> get_solutions_with_key(const vector<byte> &encrypted_message)
{
    map<char, string> decoded_strings;
    byte key = 0;
    while (true) {
        vector<byte> cipher_vec(encrypted_message.size(), key);
        BinaryOperator op;
        vector<byte> xor_vec = op.xor_arrays(encrypted_message, cipher_vec);
        string candidate = StringUtils::binary_to_string(xor_vec);
        if (StringUtils::is_printable(candidate)) {
            decoded_strings.emplace(key, move(candidate));
        }

        if (key == 255)
            break;

        key++;
    }

    return decoded_strings;
}

char FixedXorCipher::break_key(const vector<byte> &encrypted_message)
{
    map<char, string> solutions = get_solutions_with_key(encrypted_message);

    shared_ptr<MessageScore> best_score;
    char best_key;
    for (auto &solution : solutions) {
        MessageAnalyzer analyzer;
        shared_ptr<MessageScore> score = analyzer.score_as_english(solution.second);
        if (!best_score || score->is_better(*best_score)) {
            best_score = score;
            best_key = solution.first;
        }
    }

    return best_key;
}
