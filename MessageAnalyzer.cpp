#include <MessageAnalyzer.h>

map<char, double> get_expected_english_distribution()
{
    map<char, double> expected_distribution;
    expected_distribution.emplace('a', 0.08167);
    expected_distribution.emplace('b', 0.01492);
    expected_distribution.emplace('c', 0.02782);
    expected_distribution.emplace('c', 0.04253);
    expected_distribution.emplace('e', 0.12702);
    expected_distribution.emplace('f', 0.02228);
    expected_distribution.emplace('g', 0.02015);
    expected_distribution.emplace('h', 0.06094);
    expected_distribution.emplace('i', 0.06966);
    expected_distribution.emplace('j', 0.00153);
    expected_distribution.emplace('k', 0.00772);
    expected_distribution.emplace('l', 0.04025);
    expected_distribution.emplace('m', 0.02406);
    expected_distribution.emplace('n', 0.06749);
    expected_distribution.emplace('o', 0.07507);
    expected_distribution.emplace('p', 0.01929);
    expected_distribution.emplace('q', 0.00095);
    expected_distribution.emplace('r', 0.05987);
    expected_distribution.emplace('s', 0.06327);
    expected_distribution.emplace('t', 0.09056);
    expected_distribution.emplace('u', 0.02758);
    expected_distribution.emplace('v', 0.00978);
    expected_distribution.emplace('w', 0.02361);
    expected_distribution.emplace('x', 0.00150);
    expected_distribution.emplace('y', 0.01974);
    expected_distribution.emplace('z', 0.00074);
    expected_distribution.emplace(' ', 0.13000);

    return expected_distribution;
}

map<char, double> get_character_distribution(const string &message)
{
    string lower_message = message;
    transform(lower_message.begin(), lower_message.end(), lower_message.begin(), ::tolower);

    map<char, unsigned int> character_counts;
    for (char c : message) {
        if (character_counts.find(c) != character_counts.end()) {
            character_counts[c] = 0;
        }
        character_counts[c]++;
    }

    map<char, double> character_distribution;
    for (const pair<char, unsigned int> data : character_counts) {
        character_distribution.emplace(data.first, static_cast<double>(data.second) / message.length());
    }

    return character_distribution;
}

double find_or_zero(const map<char, double> &map, char c)
{
    auto it = map.find(c);
    return (it == map.end()) ? 0.0 : it->second;
}

double get_distribution_error(
    const map<char, double> &expected_distribution,
    const map<char, double> &actual_distribution)
{
    byte c = 0;
    double total_error = 0.0;
    while (true) {
        double actual = find_or_zero(actual_distribution, c);
        double expected = find_or_zero(expected_distribution, c);
        double error = fabs(actual - expected);
        total_error += error;

        if (c == 255)
            break;
        c++;
    }

    return total_error;
}

shared_ptr<MessageScore> MessageAnalyzer::score_as_english(const string &message)
{
    map<char, double> expected_distribution = get_expected_english_distribution();
    map<char, double> actual_distribution = get_character_distribution(message);
    double error = get_distribution_error(expected_distribution, actual_distribution);

    return make_shared<MessageScore>(error);
}

string MessageAnalyzer::find_best_english_string(const vector<string> &strings)
{
    string best_candidate;
    shared_ptr<MessageScore> best_score;
    for (auto candidate : strings) {
        MessageAnalyzer analyzer;
        shared_ptr<MessageScore> score = analyzer.score_as_english(candidate);
        if (!best_score || score->is_better(*best_score)) {
            best_score = score;
            best_candidate = candidate;
        }
    }

    return best_candidate;
}
