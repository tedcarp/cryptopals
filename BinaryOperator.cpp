#include <stdexcept>

#include <BinaryOperator.h>

vector<byte> BinaryOperator::xor_arrays(const vector<byte> &first, const vector<byte> &second)
{
    if (first.size() != second.size()) {
        throw invalid_argument("Can't xor vectors of mismatched sizes.");
    }

    vector<byte> result;
    for (vector<byte>::const_iterator first_it = first.begin(), second_it = second.begin();
        first_it != first.end(); first_it++, second_it++) {
        result.push_back(*first_it ^ *second_it);
    }

    return result;
}

unsigned int BinaryOperator::hamming_distance(const vector<byte> &first, const vector<byte> &second)
{
    if (first.size() != second.size()) {
        throw invalid_argument("Can't calculate hamming distance on vectors of mismatched sizes.");
    }

    unsigned int distance = 0;
    for (vector<byte>::const_iterator first_it = first.begin(), second_it = second.begin();
        first_it != first.end(); first_it++, second_it++) {
        for (unsigned int bit = 0; bit < 8; bit++) {
            unsigned int first_bit = (*first_it >> bit) & 0x1;
            unsigned int second_bit = (*second_it >> bit) & 0x1;
            if (first_bit != second_bit)
                distance++;
        }
    }

    return distance;
}
