#pragma once

#include <Common.h>

class HexCoder {
public:
    vector<byte> hex_string_to_binary(const string &hex_string);
    string binary_to_hex_string(const vector<byte> &binary_data);
};