# "all" is name of the default target, running "make" without params would use it
all: cryptopals

# for C++, replace CC (c compiler) with CXX (c++ compiler) which is used as default linker
CC=$(CXX)

CXXFLAGS = -Wall -Werror --std=c++11 -I.

DEPS = \
	Base64Encoder.h \
	BinaryOperator.h \
	Common.h \
	FixedXorCipher.h \
	HexCoder.h \
	MessageAnalyzer.h \
	RepeatingXorCipher.h \
	Set1Challenge4Data.h \
	Set1Challenge6Data.h \
	StringUtils.h
OBJ = \
	main.o \
	Base64Encoder.o \
	BinaryOperator.o \
	FixedXorCipher.o \
	HexCoder.o \
	MessageAnalyzer.o \
	RepeatingXorCipher.o \
	StringUtils.o

%.o: %.c $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

cryptopals: $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS)
