#pragma once

#include <Common.h>

class MessageScore {
  public:
    MessageScore(double score) : score(score) {}

    bool is_better(const MessageScore &other) {
        return score < other.score;
    }

  private:
    double score;
};

class MessageAnalyzer {
  public:
    shared_ptr<MessageScore> score_as_english(const string &message);
    string find_best_english_string(const vector<string> &strings);
};
